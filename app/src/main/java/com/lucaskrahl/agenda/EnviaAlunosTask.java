package com.lucaskrahl.agenda;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.lucaskrahl.agenda.converter.AlunoConverter;
import com.lucaskrahl.agenda.dao.AlunoDAO;
import com.lucaskrahl.agenda.model.Aluno;

import java.util.List;

/**
 * Created by lucas on 20/05/2017.
 */

public class EnviaAlunosTask extends AsyncTask<Object, Object, String> {
    private Context context;
    private ProgressDialog dialog;

    public EnviaAlunosTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(context, "Aguarde..", "Enviando alunos..", true, true);
    }

    @Override
    protected String doInBackground(Object ... params) {

        AlunoDAO dao = new AlunoDAO(context);
        List<Aluno> alunos = dao.buscaAlunos();
        dao.close();

        AlunoConverter conversor = new AlunoConverter();
        String json = conversor.converterParaJSON(alunos);
        WebClient client = new WebClient();

        String resposta = client.post(json);
        return resposta;
    }

    @Override
    protected void onPostExecute(String resposta) {
        Toast.makeText(context, resposta, Toast.LENGTH_LONG).show();
        dialog.dismiss();
        super.onPostExecute(resposta);
    }
}
