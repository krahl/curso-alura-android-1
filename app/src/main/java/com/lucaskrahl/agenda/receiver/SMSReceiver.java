package com.lucaskrahl.agenda.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.lucaskrahl.agenda.R;
import com.lucaskrahl.agenda.dao.AlunoDAO;

public class SMSReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            Object[] pdusObj = (Object[]) intent.getSerializableExtra("pdus");
            byte[] pdu = (byte[])pdusObj[0];
            String formato = (String) intent.getSerializableExtra("format");

            SmsMessage sms = null;
            sms = SmsMessage.createFromPdu(pdu, formato);

            String telefone = sms.getDisplayOriginatingAddress();
            AlunoDAO dao = new AlunoDAO(context);

            if(dao.eAluno(telefone)){
                Toast.makeText(context, "Chegou um SMS de Aluno!", Toast.LENGTH_SHORT).show();
                MediaPlayer m = MediaPlayer.create(context, R.raw.msg);
                m.start();
            }
            dao.close();
        }
    }
}